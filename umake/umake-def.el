;; -*- lexical-binding: t -*-

(defconst-with-prefix umake
  feature 'defmacro-gensym
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  source-files-in-order '("defmacro-gensym-utils" "defmacro-gensym")
  site-lisp-config-prefix "50")
