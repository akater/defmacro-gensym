;;; -*- lexical-binding: t; -*-

(with-no-messages (load "umake-buildenv"))

(defconst org-sources-directory (or org-local-sources-directory
                                    org-immutable-sources-directory))

(defun umake-message (format-string &rest args)
  (apply #'message (concat "%s: " format-string)
         'umake args))

(when org-local-sources-directory
    (unless (stringp org-local-sources-directory)
      (error "Error: %s is not a string: %s"
             'org-local-sources-directory org-local-sources-directory))
    (unless (string-prefix-p "/" org-local-sources-directory)
      (error "Error: Local org sources directory is ambiguous: %s"
             org-local-sources-directory))
    (umake-message "%s is set to %s"
                    'org-local-sources-directory
                    org-local-sources-directory)
    (umake-message "Relative links in el source will be overridden"))

;; don't litter (does it litter at all in batch mode?)
(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

(require 'ob-tangle)

;; this is likely only needed when org had been ripped off Emacs
;; and installed separately
(require 'org)

(require 'org-element)
(setq org-element-cache-persistent nil)

;; silence some messages
(defalias 'org-development-elisp-mode 'org-mode)

(defvar ob-flags use-flags)

(require 'ol)
(defun org--akater-redirect-link (link directory)
  (if (and (string-match org-link-types-re link)
	   (string= (match-string 1 link) "file"))
      (concat "file:"
	      (expand-file-name
               ;; basically exactly how it's done in ob-tangle
               (substring link (match-end 0))
	       directory))
    link))

(defmacro with-org-links-redirection-to (directory &rest body)
  (declare (indent 1))
  (let ((spec (gensym "spec-"))
        (directory-g (gensym "org-links-redirection-directory-")))
    `(let ((,directory-g ,directory))
       (cl-check-type ,directory-g (or null string))
       (if ,directory-g
           (cl-flet ((override-link-in-spec (,spec)
                                            (setf (caddr ,spec)
                                                  (org--akater-redirect-link
                                                   (caddr ,spec)
                                                   ,directory-g))
                                            ,spec))
             (advice-add 'org-babel-tangle-single-block
                         :filter-return #'override-link-in-spec)
             (unwind-protect (let ((org-babel-default-header-args
                                    (cons '(:comments . "link")
                                          org-babel-default-header-args)))
                               ,@body)
               (advice-remove 'org-babel-tangle-single-block
                              #'override-link-in-spec)))
         ,@body))))

(defun insert-unlicense (&optional buffer)
  (setq buffer (or buffer (current-buffer)))
  (let ((unlicense-buffer (find-file-noselect "UNLICENSE")))
    (with-current-buffer buffer
      (let ((start (point)))
        (insert-buffer-substring-no-properties unlicense-buffer)
        (comment-region start (point))))
    (kill-buffer unlicense-buffer)))

(defun insert-newline-to-please-unix () (insert ?\n))

(defun insert-elisp-postamble (file)
  (with-current-buffer (find-file-noselect file)
    (goto-char (point-max))
    (insert ?\n)
    (prin1 `(provide ',(intern (file-name-base file))) (current-buffer))
    (insert ?\n ?\n
            (file-name-nondirectory file) " ends here")
    (comment-region (line-beginning-position) (line-end-position))
    (insert-newline-to-please-unix)
    (save-buffer) (kill-buffer))
  file)

(defun usep (flag) (memq flag use-flags))

(require 'cl-extra)

(defmacro use (&rest flags)
  `(cl-every #'usep ',flags))

(if (use esdf)
    (progn
      (require 'esdf)
      ;; requiring esdf will also define some existing systems
      ;; this is a temporary hack

      (umake-message "build will use ESDF")

      ;; esdf won't delete org files
      ;; esdf won't use build dir
      ;; esdf will mix traditional install and compile phases

      (esdf-register-new-system "looking-through.esd")
      (esdf-register-new-system "looking-through-tests.esd")

      (defun umake (target)
        (umake-message "making target %s" target)
        (cl-ecase target
          (all
           ;; according to GNU standards,
           ;; this should be called check, and check should be called test
           ;; but Gentoo apparently defuaults to check rather than test
           ;; unless I've missed something
           (let ((use-flags (cons 'test use-flags)))
             (umake 'default)
             ;; todo: we should just make compile (optional?) dependency of test in esdf
             (umake 'check)))
          (clean
           (error "esdf-clean-op not implemented")
           ;; see discussion in [[file:/home/akater/src/elisp-esdf/esdf-ebuild.org::*clean and install][bug]]
           ;; (org-agenda-open-link will follow this)
           (esdf-operate 'esdf-clean-op (esdf-find-system 'looking-through)))
          (default
            (let ((esdf-org-redirect-links-to-directory org-sources-directory))
              (esdf-operate 'esdf-compile-op
                            (esdf-find-system 'looking-through)
                            ;; if this is a esdf-literate-system
                            ;; this should also work
                            ;; :tangle-options
                            ;; `( :org-redirect-links-to-directory
                            ;;    ,org-sources-directory)
                            )))
          (check
           ;; according to GNU standards,
           ;; this should be called test rather than check
           ;; but Gentoo apparently defuaults to check rather than test
           ;; unless I've missed something
           (let (
                 ;; (default-directory (expand-file-name "build"))
                 )
             ;; todo: we should just make load dependency of test in esdf
             ;; todo: we should use esdf-test-op
             (esdf-operate 'esdf-load-op
                           (esdf-find-system 'looking-through-tests)))
           (looking-through-tests-run))
          (install
           (error "esdf-ebuild-install-op not implemented")
           ;; todo: ensure `make install' works in minimalistic environment
           (esdf-operate 'esdf-ebuild-install-op
                         (esdf-find-system 'looking-through))
           (when (use test)
             (esdf-operate 'esdf-ebuild-install-op
                           (esdf-find-system 'looking-through-tests)))))
        (umake-message "made target %s" target)
        target))

  (defun umake-prepare ()
    (warn "This package is obsolete.  Use mmxx-macros instead: https://framagit.org/akater/elisp-mmxx-macros")
    (when (memq 'test use-flags)
      (push "looking-through-tests" umake-source-files-in-order))
    (cl-letf (((symbol-function 'delete-file) #'ignore))
      ;; hack borrowed from https://emacs.stackexchange.com/a/38898
      (let ((org-babel-tangle-use-relative-file-links t))
        (with-org-links-redirection-to org-sources-directory
          ;; this is a hack too
          ;; supposedly, we better would be able to specify
          ;; `org-babel-tangle-use-relative-file-links' as string,
          ;; and that string would be used as DIRECTORY argument
          ;; in `file-relative-name', in `org-babel-tangle-single-block'
          ;; any non-string might be treated
          ;; equivalently to default-directory (dynamically looked up)
          ;; btw, why is the return value of org-babel-tangle-single-block
          ;; uses (file-relative-name file) for file name
          ;; but (file-relative-name file #<something-non-nil>) for the link?
          ;; this is inconsistent
          (let (all-tangled-source-files)
            (dolist (file umake-source-files-in-order
                          (mapcar #'insert-elisp-postamble
                                  all-tangled-source-files))
              (let ((org-file (concat file ".org")))
                (dolist (tangled-file (org-babel-tangle-file org-file))
                  (unless (or (cl-member tangled-file all-tangled-source-files
                                         :test #'string-equal)
                              (string-equal "site-gentoo.d"
                                            ;; not a source file
                                            ;; but we need a better check
                                            (file-name-nondirectory
                                             (directory-file-name
                                              (file-name-directory
                                               tangled-file)))))
                    (let ((name (file-name-nondirectory tangled-file))
                          (description
                           ;; we better insert such preamble
                           ;; when tangled file is empty
                           ;; also, this is different from what esdf does:
                           ;; it searches for eponymous untangled file
                           ;; at the same level
                           ;; and takes description from it, if found
                           ;; while we take description from the first org file
                           ;; which tangled to the given file
                           (with-current-buffer (find-file-noselect org-file)
                             (when (re-search-forward
                                    (rx line-start "#+description: ")
                                    nil t)
                               (buffer-substring-no-properties
                                (point) (line-end-position)))))
                          (local-variables '(:lexical-binding t)))
                      (with-current-buffer (find-file-noselect tangled-file)
                        (goto-char (point-min))
                        (let ((start (point)))
                          (insert name)
                          (when description (insert " --- " description))
                          (when local-variables
                            (insert "  " "-*- ")
                            (cl-loop
                             for (key value) on local-variables by #'cddr
                             do (insert (ensure-string key) ": "
                                        (ensure-string value)))
                            (insert " -*-"))
                          (insert ?\n)
                          (comment-region start (point)))
                        (insert ?\n)
                        (insert-unlicense)
                        (save-buffer)))
                    (push tangled-file all-tangled-source-files)))))))))
    'prepared)

  (defun umake-config ()
    ;; when site-lisp-configuration is absent,
    ;; any config beyond in-source autoloads is discarded
    (when autoloads-file
      ;; I generally do not `require' in defuns
      ;; but this file is one-time init
      (require 'autoload)
      (let ((generated-autoload-file (expand-file-name
                                      (if site-autoloads
                                          (format "site-gentoo.d/%s-gentoo.el"
                                                  umake-feature)
                                        autoloads-file))))
        (with-current-buffer (find-file-noselect generated-autoload-file)
          (ensure-directory default-directory)
          (save-buffer)                 ; file should exist
          (if (not site-autoloads) (progn (kill-buffer)
                                          (update-directory-autoloads
                                           default-directory))
            (let ((inhibit-read-only t))
              (goto-char (point-min))
              (insert ?\n
                      (format ";;; %s site-lisp configuration"
                              umake-feature)
                      ?\n ?\n)
              (prin1 `(add-to-list 'load-path ,(expand-file-name
                                                (ensure-string umake-feature)
                                                "/usr/share/emacs/site-lisp/"))
                     (current-buffer))
              (insert ?\n ?\n)
              (goto-char (point-max))
              (insert ?\n
                      (format ";;; begin: forms written by `%s'"
                              'autoload-generate-file-autoloads)
                      ?\n)
              (save-buffer)
              (let ((default-directory (expand-file-name ".." default-directory)))
                (do-default-directory-files (el-file (rx ".el" string-end) t t)
                  ;; todo: deal with the case when el-file
                  ;; specifies generated-autoload-file
                  (insert ?\n)
                  (let ((generated-autoload-load-name (file-name-base el-file)))
                    (autoload-generate-file-autoloads el-file (current-buffer)))))
              (insert ?\n
                      (format ";;; end: forms written by `%s'"
                              'autoload-generate-file-autoloads)
                      ?\n))
            (save-buffer) (kill-buffer)))))
    'autoloads)

  (defvar umake-compile-error nil)

  (defun umake-compile ()
    (do-default-directory-files (el-file (rx ".el" string-end) t
                                         t ;; order of compilation is arbitrary
                                         ;; according to
                                         ;;   File: elisp.info
                                         ;;   Node: Multi-file Packages
                                         )
      "Compiled %s file%s"
      (if (string-equal "sitefile" (file-name-base el-file)) (do-not-count)
        (unless (byte-compile-file el-file)
          (do-not-count)
          (setf umake-compile-error t))))
    (umake-config)
    (unless umake-compile-error
      'compiled))

  (defun umake (target &optional live)
    "Kill Emacs in the end (with appropriate error code) when LIVE is non-nil."
    (when live
      (cl-assert target "Null target doesn't allow distinguishing errors and non-errors when LIVE is non-nil"))
    (let ((umake-compile-error umake-compile-error))
      (umake-message "making target %s" target)
      (cl-ecase target
        (all
         (let ((use-flags (cons 'test use-flags)))
           (umake 'default)
           (umake 'check)))
        (clean (do-default-directory-files (el-or-elc-file
                                            (rx ".el" (zero-or-one ?c) string-end)
                                            nil t)
                 "Deleted %s source (el) or byte-compiled (elc) file%s"
                 (delete-file el-or-elc-file))
               (delete-directory "site-gentoo.d" t))
        (default
          (umake-prepare)
          (umake-compile))
        (check
         (error "Tests not implemented")
         (let ((default-directory (ensure-directory (expand-file-name "tests"))))
           (load (expand-file-name "looking-through-tests")))
         (ebuild-run-mode-tests-run))
        ;; (install
        ;;  (umake-install))
        )
      (if live
          (unless umake-compile-error
            (umake-message "made target %s" target)
            target)
        ;; we return 1 for compile-error
        ;; because that's what batch-byte-compile-file returns
        (kill-emacs (if umake-compile-error 1 0))))))

